﻿(() => {
    'use strict';

    angular
        .module('app.chat')
        .factory('chatservice', chatservice);

    chatservice.$inject = ['$rootScope', '$log'];

    function chatservice($rootScope, $log) {
        var hub = $.connection.chatHub;

        var service = {
            onMessage: _onMessage,
            send: _send,
            start: _start,
            createMessage: _createMessage
        };

        return service;

        function _onMessage(callback) {
            hub.client.onMessage = (message) => {
                callback(message);
                $rootScope.$apply();
            }

            return service;
        }

        function _send(message) {
            hub.server.send(message);
        }

        function _start() {
            $.connection.hub.start().done(() => {
                $log.log('Connection started');
            });
        }

        function _createMessage(spec) {
            var _user = spec.user || '',
                _text = spec.text || '',
                _date = spec.date || new Date();

            return {
                user: _user,
                text: _text,
                date: _date
            };
        }
    }
})();