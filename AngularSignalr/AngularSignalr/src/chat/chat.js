﻿(() => {
    'use strict';

    angular
        .module('app.chat')
        .component('chat', {
            templateUrl: '/src/chat/chat.html',
            controller: Chat,
            controllerAs: 'vm',
            bindings: {
                user: '='
            }
        });

    Chat.$inject = ['chatservice'];

    function Chat(service) {
        var vm = this;

        vm.message = null;
        vm.messages = [];
        vm.user = null;
        vm.onMessage = _onMessage;
        vm.send = _send;

        activate();

        function activate() {
            service
                .onMessage(vm.onMessage)
                .start();
        }

        function _onMessage(message) {
            _addMessage(message);
        }

        function _send() {
            if (!vm.message) return;

            var message = service.createMessage({
                user: vm.user,
                text: vm.message,
                date: new Date()
            });

            service.send(message);

            vm.message = null;
        }

        function _addMessage(message) {
            if (!message) return;

            vm.messages.push(`${message.date} - ${message.user}: ${message.text}`);
        }
    }
})();