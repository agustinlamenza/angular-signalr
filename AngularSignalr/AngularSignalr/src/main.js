﻿(() => {
    'use strict';

    angular
        .module('app', [
            'app.root',
            'app.chat'
        ]);
})();