﻿using AngularSignalr.Models;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSignalr.Hubs
{
    public interface IChatHub
    {
        void OnMessage(Message message);
    }
}
