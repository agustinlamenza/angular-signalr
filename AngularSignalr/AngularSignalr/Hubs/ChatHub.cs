﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using AngularSignalr.Models;

namespace AngularSignalr.Hubs
{
    [HubName("chatHub")]
    public class ChatHub : Hub<IChatHub>
    {
        [HubMethodName("send")]
        public void Send(Message message)
        {
            Clients.All.OnMessage(message);
        }
    }
}