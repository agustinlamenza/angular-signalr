﻿using System.Web;
using System.Web.Optimization;

namespace AngularSignalr
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendor-build")
                .Include("~/Scripts/jquery-{version}.js")
                .Include("~/Scripts/jquery.signalR-{version}.js")
                .Include("~/Scripts/angular.js"));

            bundles.Add(new ScriptBundle("~/bundles/app-build")
                .IncludeDirectory("~/src", "*.module.js", true)
                .IncludeDirectory("~/src", "*.js", true));

            //BundleTable.EnableOptimizations = true;
        }
    }
}
